# README #

### What is this repository for? ###
This repository contains C++ implementation of Space Filling Curves for 2D and 3D block meshes. 


### How do I get set up? ###
The folder contains SFC.cpp which should be compiled by #  g++ -std=c++11 -O3  # options and it will generate two files without_order.dat and 
with_order.dat which can be visualized with gnupplot using command given in the following lines

# plot "without_order.dat" u 1:2 with lines _and similarly for with order.

### Contribution guidelines ###
This work is done at Technical University of Delft, Numerical Analysis. 

### Who do I talk to? ###

sagardolas.cosse@gmail.com